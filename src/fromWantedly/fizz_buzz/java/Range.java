package fromWantedly.fizz_buzz.java;

import java.util.ArrayList;

public class Range {
    String SEPARATOR = System.getProperty("line.separator");
    int min;
    int max;

    final ArrayList<Alias> n_Alias; // all AND_operation

    public Range(int min, int max) {
        this.update(min,max);
        this.n_Alias = new ArrayList<>();
    }

    void update(int min, int max) {
        this.min = min;
        this.max = max;
    }

    Alias aliasAt(int id) {
        for (Alias target : this.n_Alias) {
            if (target.basis == id) {
                return target;
            }
        }
        return null;
    }

    String editedList() {
        final StringBuilder list = new StringBuilder();
        boolean savedAlias;
        for (int suspect = this.min; suspect <= this.max; suspect++) {
            savedAlias = false;

            for (Alias alias : this.n_Alias) {
                if(alias.rewritten(suspect,list)) {
                    savedAlias = true;
                }
            }

            if (! savedAlias) {
                list.append(suspect);
            }
            list.append(SEPARATOR);
        }

        return list.toString();
    }

    public static void main(String[] args) {
        Range range = new Range(1,100);

        range.n_Alias.add(new Alias(3,"Fizz"));
        range.n_Alias.add(new Alias(5,"Buzz"));

        System.out.println("---　BASIC question　( Multiple ) -------");

        range.aliasAt(3).n_Condition.add(new Multiple());
        range.aliasAt(5).n_Condition.add(new Multiple());

        System.out.println(range.editedList());

        System.out.println("---　APPLIED question　( Multiple ) && ( Included )-------");

        range.aliasAt(3).n_Condition.add(new Included());
        range.aliasAt(5).n_Condition.add(new Included());

        System.out.println(range.editedList());

        System.out.println("------　Final question　-------");
//        targetRange.update(100, 1000000);
////
//        System.out.println(targetRange.editedList());








    }
}
