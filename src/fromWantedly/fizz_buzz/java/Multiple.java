package fromWantedly.fizz_buzz.java;

public class Multiple implements ORCondition {
    @Override
    public boolean satisfied(int basis, int suspect) {
        return suspect % basis == 0;
    }
}
