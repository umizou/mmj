package fromWantedly.fizz_buzz.java;

public class Included implements ORCondition {

    boolean test(int basis, int target) {

        if(target < basis) {
            return false;
        }
        int nextTarget = target / 10;

        return (target - nextTarget * 10 == basis)? true : test(basis,  nextTarget);

    }

    @Override
    public boolean satisfied(int basis, int suspect) {
        int quotient;

        do{
            quotient = suspect / 10;
            // calc a new remainder
            if(suspect - (10 * quotient) == basis) {
                return true;
            }
        }while ((suspect = quotient) >= basis);


        return false;
    }

    public static void main(String[] args) {
        Included i = new Included();

//        for (int j = 31; j < 101; j++) {
//            System.out.println(i.satisfied(3,j));
//        }

        System.out.println(i.satisfied(3,3));
        System.out.println(i.satisfied(3,12));
        System.out.println(i.satisfied(3,13));
        System.out.println(i.satisfied(3,139));
        System.out.println(i.satisfied(3,129));

        System.out.println(i.test(3,3));
        System.out.println(i.test(3,12));
        System.out.println(i.test(3,13));
        System.out.println(i.test(3,139));
        System.out.println(i.test(3,129));


    }


}
