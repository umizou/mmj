package fromWantedly.fizz_buzz.kotlin

abstract class Cndt(val basis : Int) {
    abstract fun satisfied(target:Int):Boolean
}

class Mltpl(basis: Int):Cndt(basis) {

    // test for Master Branch
    override fun satisfied(target: Int):Boolean {
        return target % this.basis == 0
    }
}

class Incldd(basis: Int):Cndt(basis) {

//    var radix:Int

    init {
//        this.radix = String.format(1000).length()
    }

    // test recursion
    private fun calcDigit(num:Int) :Int{
        return 2


//        while () {
//
//        }



//        val digit : Double = Integer.toString(10).length
//        val radix : Int = Math.pow(10.0,digit)

    }

    private fun radix(digit:Int):Int{
        return 10
    }

    // ^^; basisが二桁の場合、適切に機能しない　-> target についても全ての桁についてチェックする？
    // Calculate a ShimoHitoketa to judge
    override tailrec fun satisfied(target: Int): Boolean {
        val radix = 10

        if (target < basis) return false
        val nextTarget = target / radix
        return if (target - nextTarget * radix == basis) true else satisfied(nextTarget)
    }
}

fun main(args: Array<String>) {

    println(Integer.toString(10).length)

    var condition:Cndt = Mltpl(3)
    println(condition.satisfied(6))

//    condition= Incldd(3)

    val inc = Incldd(19)

    for (target in 1..100){
        if(inc.satisfied(target)) {
            println(true)
        }else{
            println(target)
        }
//        println("$target" "condition.satisfied(target)")
    }


}