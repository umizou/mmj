package fromWantedly.fizz_buzz.java;

//import fromWantedly.sample_04.Judgement;

import java.util.ArrayList;

public class Alias {
    int basis;
    String info;
    final ArrayList<ORCondition> n_Condition;

    public Alias(int basis, String info) {
        this.basis = basis;
        this.info = info;
        this.n_Condition = new ArrayList<>();
    }

    boolean rewritten(int suspect, StringBuilder list) {
        for(ORCondition condition : this.n_Condition) {
            if(condition.satisfied(this.basis, suspect)) {
                list.append(this.info);
                return true;
            }
        }
        return false;

    }

    public static void main(String[] args) {
        StringBuilder list = new StringBuilder();
        Alias a =  new Alias(5,"Buzz");
        a.n_Condition.add(new Multiple());
        a.n_Condition.add(new Included());

        for (int i = 1; i < 101; i++) {
            if(! a.rewritten(i,list)) {
                list.append(i);
            }
//            editedList.append(Judgement.separator);

        }
        System.out.println(list.toString());
    }

}
