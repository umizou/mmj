package fromWantedly.fizz_buzz.kotlin


// test for check out last ver 12:25
// [ 1 ]
// [ 2 ]
class Als(val info:String) {
    val nCndt:MutableList<Cndt> = mutableListOf()


    fun rewritten(target:Int, list:StringBuilder):Boolean{
        for (condition in this.nCndt) {

            if (condition.satisfied(target)) {
                list.append(this.info)
                return true     // for OR Operation
            }
        }
        return false
    }




}

fun main(args: Array<String>) {
    val als:Als = Als("FizzK")

    als.nCndt.add(Mltpl(3))
    als.nCndt.add(Incldd(3))

    val editedList = StringBuilder()
    for (target in 1..100) {
        if (! als.rewritten(target,editedList)) {
            editedList.append(target)
        }
        editedList.append("\n")
    }
    println(editedList.toString())
}
