package fromWantedly.fizz_buzz.java;

// all AND_operation
public interface ORCondition {
    boolean satisfied(int basis, int suspect);
}
